const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const http = require("http")
const connectFlash = require("connect-flash")
const cookie = require("cookie-session")
//database
const mongoose = require("mongoose");
const database = require("./config/database")

const upload_file = require('./routes/upload_file');
const index = require('./routes/index');
const panel = require('./routes/panel/main');
const events = require('./routes/panel/events');
const delete_event = require('./routes/panel/delete_event');
const create_event = require('./routes/panel/event_wizard');
const edit_event = require('./routes/panel/edit_event_wizard');
const login = require('./routes/panel/login');
const register = require('./routes/panel/register');
const verification = require('./routes/verification');
const logout = require('./routes/logout')
const billing = require('./routes/billing/charge');
const plans = require('./routes/panel/plans');
const transactions = require('./routes/panel/transactions');
const withdraw = require('./routes/panel/withdraw');
const settings = require('./routes/panel/settings');
const support = require('./routes/panel/support');
const create_ticket_support = require('./routes/panel/create_ticket')
const about = require('./routes/about');
const api = require('./routes/api');

// event ticket buying
const view_events = require('./routes/events/view');
const purchase_ticket = require('./routes/events/purchase');
const charge = require('./routes/events/charge');

mongoose.Promise = Promise;  
mongoose.connect(database.url);

mongoose.connection.on('connected', function () {
  console.log('Mongoose default connection open');
});

// If the connection throws an error
mongoose.connection.on('error',function (err) {
  console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  console.log('Mongoose default connection disconnected');
});

var app = express();

app.use(cookie({
  name: 'session',
  keys: ["key123", "key456"],
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}))
var fileUpload = require("express-fileupload")
app.use(fileUpload({
  limits: { fileSize: 50 * 1024 * 1024 },
}));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

const keyPublishable = "pk_test_d3fJOZGJcjOPq7OSgrp3NoUv";
const keySecret = "sk_test_BrE1atluOJpmKZBqDfOXNFcX";

const stripe = require("stripe")(keySecret);

app.locals.stripe = stripe;
app.locals.keyPublishable = keyPublishable;

var accessAPI = function(req, res, next) {
  if(req.session) {
    if(req.session.email) {
      next()
      return;
    }
  }
  res.status(400).send({"error" : "Unauthorized"})
}

var access = function(req, res, next) {
  if(req.session) {
    if(req.session.email) {
      next()
      return;
    }
  }
  res.redirect("/")
}

var deny = function(req, res, next) {
  if(req.session) {
    if(req.session.email) {
      res.redirect('/')
      return;
    }
  }
  next()
}

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(connectFlash())
app.use('/', index);
app.use('/about', about);
app.use('/logout', logout)
app.use('/billing', access, billing)
app.use('/panel', access, panel);
app.use('/panel/plans', access, plans)
app.use('/panel/transactions', access, transactions)
app.use('/panel/withdraw', access, withdraw)
app.use('/panel/my_events', access, events);
app.use('/panel/my_events/delete', access, delete_event);
app.use('/panel/my_events/create', access, create_event);
app.use('/panel/my_events/edit', access, edit_event);
app.use('/panel/account_settings', access, settings);
app.use('/panel/support', access, support);
app.use('/support/create_ticket', access, create_ticket_support);
app.use('/login', deny, login);
app.use('/register', deny, register);
app.use('/verification', deny, verification);

app.use('/api/v1', api);

app.use('/event_handler/view_event', view_events)
app.use('/event_handler/purchase_ticket', purchase_ticket)
app.use('/event_handler/checkout', charge)

app.use('/upload_file', upload_file);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', {
    title: "Error: " + err.status == undefined ? 500 : err.status
  });
});

var EventEmitter = require('events').EventEmitter;
app.locals.eventEmitter = EventEmitter

var port = process.env.PORT;
app.set('port', port);

var server = http.createServer(app);

var socket = require("./socket")(server, EventEmitter)

server.listen(process.env.PORT, process.env.IP, function(){
  console.log("Server has started");
})

module.exports = app;