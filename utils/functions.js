var User = require("../routes/models/user");
var Event = require("../routes/models/event")
var Ticket = require("../routes/models/ticket")

const months = {
    january: 0,
    february: 0,
    march: 0,
    april: 0,
    may: 0,
    june: 0,
    july: 0,
    august: 0,
    september: 0,
    october: 0,
    november: 0,
    december: 0
}

const month_from_number = {
    0: "january",
    1: "february",
    2: "march",
    3: "april",
    4: "may",
    5: "june",
    6: "july",
    7: "august",
    8: "september",
    9: "october",
    10: "november",
    11: "december"
}

var checkDateDay = function(date) {
    var inputDate = new Date(date);

    // Get today's date
    var todaysDate = new Date();
    
    // call setHours to take the time out of the comparison
    if(inputDate.getDay() == todaysDate.getDay()) {
        return true
    } else {
        return false
    }
}

var checkDateMonth = function(date) {
    var inputDate = new Date(date);

    // Get today's date
    var todaysDate = new Date();
    
    // call setHours to take the time out of the comparison
    if(inputDate.getMonth() == todaysDate.getMonth()) {
        return true
    } else {
        return false
    }
}

module.exports = {
    
    verifySubscription: function(doc, sub_id, req, fn) {
        var stripe = req.app.locals.stripe;
        stripe.subscriptions.list(
            {"customer" : doc.customerToken},
            function(err, subscriptions) {
              if(err) {
                fn(err, null)
              } else {
                for(var i = 0; i < subscriptions.data.length; i++) {
                  var sub = subscriptions.data[i]
                  if(sub.id == sub_id) {
                    fn(null, true)
                    return;
                  }
                  
                  if(i+1 == subscriptions.data.length) {
                      fn(null, false)
                  }
                }
              }
          });
    },
    getUser: function(email, fn) {
        User.findOne({"email" : email}, function(err, doc) {
            if(!err && doc) {
                fn(doc)
            } else {
                fn(null)
            }
        })
    },
    getUserID: function(id, fn) {
        User.findOne({_id : id}, function(err, doc) {
            if(!err && doc) {
                fn(doc)
            } else {
                fn(null)
            }
        })
    },
    checkEmail: function(email, fn) {
        User.find({}, function(err, docs) {
            docs.forEach(function(doc) {
                if(doc && doc.email == email) {
                    fn(true)
                    return;
                }
            })
            fn(false)
        })
    },
    verifyEmail: function(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email.toLowerCase());
    },
    verifyUser: function(email, password, fn) {
        var utils = require("./functions")
        utils.getUser(email, function(user) {
            if(user != null) {
                if(user.verification.redeemed == true) {
                    user.comparePassword(password, function(err, isMatch) {
                        if(!err && isMatch) {
                            fn(null, true)
                        } else {
                            fn(err, null)
                        }
                    })
                } else {
                    fn("Please verify your account", null)
                }
            } else {
                fn("Invalid email or password.", null)
            }
        })
    },
    getUserEvents: function(doc, fn) {
        var id = doc._id;
        Event.find({"author" : id}, function(err, doc) {
            if(!err && doc) {
                var a = new Promise(function(resolve) {
                    var arr = []
                    if(doc.length <= 0) {
                        resolve([])
                    }
                    for(var i = 0; i < doc.length; i++) {
                        const event = doc[i]
                        const max = event.max_tickets
                        Ticket.find({"author" : event._id}, function(err, ev) {
                            var e = {
                                sold: ev == undefined ? 0 : ev.length,
                                _id: event._id,
                                created_at: event.created_at,
                                updated_at: event.updated_at,
                                location: event.location,
                                author: event.author,
                                name: event.name,
                                start: event.start,
                                end: event.end,
                                maximum_tickets: max
                            }
                            arr.push(e)
                            
                            if(arr.length == doc.length) {
                                resolve(arr)
                            }
                        })

                    }
                })
                
                Promise.all([a]).then(function(arr) {
                    fn(null, arr[0])   
                })
            } else {
                fn(err, doc)
            }
        })
    },
    getTicketSoldEvent: function(id) {
        Ticket.find({"author" : id}, function(err, doc) {
            if(!err && doc) {
                return doc.length;
            } else {
                return 0;
            }
        })
        
        return 0;
    },
    getEvent: function(event, fn) {
        Event.findOne({_id : event}, function(err, doc) {
            if(!err && doc) {
                fn(doc)
            } else {
                fn(undefined)
            }
        })
    },
    reqUser: function(req, fn) {
        var utils = require("./functions")
        if(req.session && req.session.email) {
          utils.getUser(req.session.email, function(doc) {
            if(doc != null) {
              fn({
                  email: doc.email,
                  customerToken: doc.customerToken,
                  stripeCustomerToken: doc.stripeCustomerToken,
                  balance: doc.balance,
                  country: doc.country
              })
            } else {
              fn(undefined)
            }
          })
        } else {
          fn(undefined)
        }
    },
    reqUserAll: function(req,resolve) {
        var utils = require("./functions")
        if(req.session && req.session.email) {
          utils.getUser(req.session.email, function(doc) {
            if(doc != null) {
              resolve({
                  email: doc.email,
                  _id: doc._id,
                  customerToken: doc.customerToken,
                  stripeCustomerToken: doc.stripeCustomerToken,
                  balance: doc.balance,
                  country: doc.country
              })
            } else {
              resolve(null)
            }
          })
        } else {
          resolve(null)
        }
    },
    getTotalTicketsToday: function(author, cb) {
        Ticket.find({user_author: author}, function(err, doc) {
            if(!err && doc && doc.length > 0) {
                var promise = new Promise(function(resolve, reject) {
                    var docs = []
                    for(var i = 0; i < doc.length; i++) {
                        var ticket = doc[i]
                        if(checkDateDay(ticket.created_at)) {
                            docs.push(ticket)
                        }
                        
                        if(i+1 == doc.length) {
                            resolve(docs)
                        }
                    }
                })
                promise.then(function(resolve) {
                    cb(null, resolve.length)
                })
            } else {
                cb(err, 0)
            }
        })
    },
    getTotalTicketsMonth: function(author, cb) {
        Ticket.find({user_author: author}, function(err, doc) {
            if(!err && doc && doc.length > 0) {
                var promise = new Promise(function(resolve, reject) {
                    var docs = []
                    for(var i = 0; i < doc.length; i++) {
                        var ticket = doc[i]
                        if(checkDateMonth(ticket.created_at)) {
                            docs.push(ticket)
                        }
                        
                        if(i+1 == doc.length) {
                            resolve(docs)
                        }
                    }
                })
                promise.then(function(resolve) {
                    cb(null, resolve.length)
                })
            } else {
                cb(err, 0)
            }
        })
    },
    getTotalTicketsAllTime: function(author, cb) {
        Ticket.find({user_author: author}, function(err, doc) {
            if(!err && doc && doc.length > 0) {
                cb(null, doc.length)
            } else {
                cb(err, 0)
            }
        })
    },
    getYearStats: function(author, cb) {
        Ticket.find({user_author: author}, function(err, doc) {
            if(!err && doc && doc.length > 0) {
                var promise = new Promise(function(resolve) {
                    for(var i = 0;i < doc.length; i++) {
                        var a = doc[i]
                        var t_date = new Date(doc.created_at)
                        
                        for(var e = 0; e < 12; e++) {
                            if(t_date.getMonth == e) {
                                months[month_from_number[e]] = months[month_from_number[e]] + 1;
                            }
                        }
                        
                        if(i+1 == doc.length) {
                            resolve()
                        }
                    }
                })
                promise.then(function() {
                    cb(months)
                })
            } else {
                cb(months)
            }
        })
    },
    validateFields: function(fields) {
        for(var i = 0; i < fields.length; i++) {
            var field = fields[i]
            if(field == "" || field == undefined) {
                return false
            }
            
            if(i+1 == fields.length) {
                return true;
            }
        }
    },
    soldOut: function(id, max, fn) {
        Ticket.find({author: id}, function(err, doc) {
            if(!err && doc) {
                fn(doc.length == max, doc.length)
            } else {
                fn(true, max)
            }
        })
    },
    getBalances: function(balance, fn) {
        
        var available = 0;
        var pending = 0;
        
        new Promise(function(resolve, reject) {
            for(var i = 0; i < balance.pending.length; i++) {
                pending = pending + parseInt(balance.pending[i].amount);
                  
                if(i+1 == balance.pending.length) { resolve() }
            }
        }).then(new Promise(function(resolve, reject) {
            for(var i = 0; i < balance.available.length; i++) {
                available = available + parseInt(balance.available[i].amount);
                  
                if(i+1 == balance.available.length) { resolve() }
            }
        })).then(function() {
            fn({
                available: available,
                pending: pending
            })
        })
        
    },
    getSpecialEvents: function(fn) {
        var random = new Promise(function(resolve, reject) {
            Event.count().exec(function (err, count) {
        
              // Get a random entry
                var random = Math.floor(Math.random() * count)
                    
                console.log(random)
                
                  // Again query all users but only fetch one offset by our random #
                Event.findOne().skip(random).exec( function (err, result) {
                    resolve(result)
                })
            })              
        })
        
        
        var recent = new Promise(function(resolve, reject) {
            Event.find({}, {}, { sort: { 'created_at' : -1 } }).limit(10).exec(function(err, post) {
              resolve(post)
            });
        })
        
        Promise.all([random, recent]).then(function(res) {
            var random = res[0]
            var recent = res[1]
            var obj = {random: random, recent: recent}
            fn(obj)
        })
    }
}