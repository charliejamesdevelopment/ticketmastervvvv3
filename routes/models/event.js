var mongoose = require('mongoose');
var bcrypt = require("bcrypt")
var Schema = mongoose.Schema;

var event = new Schema({
  name: String,
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'author'
  },
  cover_image: String,
  max_tickets: Number,
  location: String,
  description: String,
  start: Date,
  end: Date,
  tickets: [{
    name: String,
    price: Number,
    description: String
  }],
  created_at: Date,
  updated_at: Date
});

event.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at) this.created_at = currentDate;
  
  next()

});

event.set('collection', 'events');

var Event = mongoose.model('Event', event);

// make this available to our events in our Node applications
module.exports = Event;