var mongoose = require('mongoose');
var bcrypt = require("bcrypt")
var Schema = mongoose.Schema;

var support = new Schema({
  buyer: String,
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'author'
  },
  message: String,
  subject: String
});

support.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at) this.created_at = currentDate;
  
  next()

});

support.set('collection', 'support_tickets');

var Support = mongoose.model('Support', support);

// make this available to our events in our Node applications
module.exports = Support;