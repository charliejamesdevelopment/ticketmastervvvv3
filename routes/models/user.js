var mongoose = require('mongoose');
var bcrypt = require("bcrypt")
var Schema = mongoose.Schema;

var user = new Schema({
  country: String,
  stripeCustomerToken: String,
  email: String,
  password: String,
  verification: {
    code: String,
    redeemed: Boolean
  },
  created_at: Date,
  updated_at: Date,
  customerToken: String
});

user.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(10, function(err, salt) {
        if (err) return next(err);

        // hash the password along with our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

user.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};


user.set('collection', 'users');

var User = mongoose.model('User', user);

// make this available to our users in our Node applications
module.exports = User;