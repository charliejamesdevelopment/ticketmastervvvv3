var express = require('express');
var functions = require('../utils/functions')
var Event = require('./models/event');
var router = express.Router();

router.get("/get_user/:id", (req, res) => {
  var stripe = req.app.locals.stripe;
  var stripeCustomerToken = req.params.id;
    stripe.accounts.retrieve(
        stripeCustomerToken,
        function(err, account) {
          if(!err && account) {
            res.status(200).send(account)
          }
        }
    );  
})

router.get("/get_types/:id", (req, res) => {
  functions.getEvent(req.params.id, function(event) {
    if(event) {
      res.status(200).send(event.tickets)
    } else {
      res.status(400).send({"error" : "Could not find event."})
    }
  })
})

module.exports = router;