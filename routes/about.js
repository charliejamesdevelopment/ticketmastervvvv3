var express = require('express');
var functions = require('../utils/functions')
var router = express.Router();

router.get('/', function(req, res, next) { 
  functions.reqUser(req, function(doc) {
    res.render('about', { 
      title: 'About', 
      route: req.originalUrl,
      error: req.flash('error'),
      success: req.flash('success'),
      user: doc
    });
  });
});

module.exports = router;
