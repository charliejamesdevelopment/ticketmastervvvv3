var express = require('express');
var router = express.Router();
var functions = require('../../utils/functions')

/* GET users listing. */
router.get('/', function(req, res, next) {
  functions.reqUserAll(req, function(doc) {
    res.render('user/support', { 
      title: 'Support', 
      description: 'View your tickets and create a ticket!',
      route: req.originalUrl,
      error: req.flash('error'),
      success: req.flash('success'),
      user: doc
    }); 
  });
});

module.exports = router;
