var express = require('express');
var router = express.Router();
var functions = require('../../utils/functions')
var validate = require("validate.js")
var Event = require("../models/event")

/* GET users listing. */
router.get('/', function(req, res, next) {
  functions.reqUser(req, function(doc) {
    res.render('user/create_event_wizard', { 
      title: 'Events', 
      description: 'Create an Event with our wizard.',
      route: req.originalUrl,
      error: req.flash('error'),
      success: req.flash('success'),
      user: doc
    });
  });
});

router.post('/', function(req, res, next) {
  var name = req.body.name
  var description = req.body.description
  var total_tickets = req.body.total_tickets
  var max_tickets = req.body.max_tickets
  var start = req.body.start
  var end = req.body.end
  var location = req.body.location;
  var cover_image = req.body.cover_image;
  
  if(name != "" && total_tickets != "" && max_tickets != "" && start != "" && end != "") {
      var constraints = {
        name: {length: {minimum: 5, maximum: 64}},
        location: {length: {minimum: 3, maximum: 64}}
      };
      var loc_val = validate({location: location}, constraints);
      if(loc_val != undefined) {
          req.flash("error", location.location)
          res.redirect("/panel/my_events/create")
          return;
      }
      var tickets = []
      var promise = new Promise(function(resolve, reject) {
        for(var i = 1; i <= total_tickets; i++) {
          var name = req.body["ticket_name_" + i]
          var desc = req.body["ticket_desc_" + i]
          var price = req.body["ticket_price_" + i]
          if(name != "" && desc != "" && price != "") {
            
            var val_name = validate({name: name}, constraints);
            var val_price = validate({price: price}, {price: {numericality: true}});
            var val_max = validate({max_tickets: max_tickets}, {max_tickets: {numericality: true}});
            if(val_max != undefined) {
              reject("Please make sure max tickets is a integer.")
            }
            if(val_name != undefined && val_price != undefined) {
              reject("Make sure the name of a ticket is more than 5 characters, and less than 64. Also make sure price is an integer.")
            } else {
              tickets.push({
                name: name,
                description: desc,
                price: price
              })
              
              if(i == total_tickets) {
                resolve()
              }
            }
          } else {
            reject("Please fill in all fields for Step 2.")
          }
        }
      });
      
      promise.then(function(resolve) {
        functions.reqUserAll(req, function(doc) {
          var ev = {
            cover_image: cover_image,
            location: location,
            author: doc._id,
            name: name,
            start: start,
            end: end,
            description: description,
            tickets: tickets,
            max_tickets: max_tickets
          }
          var eq = Event(ev)
          
          eq.save(function(err) {
            if(!err) {
              req.flash("success", "Your event has been created.")
              res.redirect("/panel/my_events/")
            } else {
              console.log(err)
            }
          })
          
        })
      }).catch(function(reject) {
          if(reject) {
            req.flash("error", reject)
            res.redirect("/panel/my_events/create")
          }
        })
  } else {
    req.flash("error", "Please fill in all fields for Step 1.")
    res.redirect("/panel/my_events/create")
  }
});

module.exports = router;
