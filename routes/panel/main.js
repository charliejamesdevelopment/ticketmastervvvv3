var express = require('express');
var router = express.Router();
var functions = require('../../utils/functions')

/* GET users listing. */
router.get('/', function(req, res, next) {
  functions.reqUserAll(req, function(doc) {
    var promise = new Promise(function(resolve, reject) {
      var obj = {}
      
      functions.getTotalTicketsMonth(doc._id, function(err, a) {
        obj.month = a;
        functions.getTotalTicketsAllTime(doc._id, function(err, b) {
          obj.all = b;
          functions.getTotalTicketsToday(doc._id, function(err, c) {
            obj.today = c;
            resolve(obj)
          })
        })
      })
    })
    
    promise.then(function(resolve) {
      res.render('user/panel', { 
        title: 'Panel', 
        description: 'Your dashboard for all your events and buyers.',
        route: req.originalUrl,
        error: req.flash('error'),
        success: req.flash('success'),
        user: doc,
        stats: resolve
      });      
    })
  });
});

module.exports = router;
