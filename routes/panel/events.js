var express = require('express');
var router = express.Router();
var functions = require('../../utils/functions')

/* GET users listing. */
router.get('/', function(req, res, next) {
  functions.reqUserAll(req, function(doc) {
    functions.getUserEvents(doc, function(err, events) {
      new Promise(function(resolve, reject) {
        var n = []
        if(events.length != 0){
          events.forEach(function(event) {
            n.push(event)
          })
        } else {
          resolve([])
        }
        resolve(n)
      }).then(function(resolve) {
        var newEvents = resolve;
        if(err) req.flash("error", "Something went wrong while trying to retrieve your events.")
        res.render('user/events', { 
          title: 'Events', 
          description: 'Manage and modify all your events.',
          route: req.originalUrl,
          error: req.flash('error'),
          success: req.flash('success'),
          user: doc,
          events: newEvents
        });
      })
    })
  });
});

module.exports = router;
