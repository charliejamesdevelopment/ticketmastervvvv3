var express = require('express');
var functions = require('../../utils/functions')
var User = require("../models/user")
var uniqid = require('uniqid');
var nodemailer = require('../../utils/nodemailer');
var validate = require('validate.js')
var moment = require('moment')

var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('user/register', { 
    title: 'Register', 
    route: req.originalUrl,
    error: req.flash('error'),
    success: req.flash('success'),
    user: null
  });
});

router.post('/', function(req, res, next) {
  var stripe = req.app.locals.stripe;
  var email = req.body.email;
  var password = req.body.password;
  var confirm = req.body.confirm_password;
  var country = req.body.country == "" ? "US" : req.body.country;
  var first_name = req.body.first_name;
  var last_name = req.body.last_name;
  var type = req.body.type;
  var dob = moment(req.body.dob);
  
  if(functions.validateFields([email, password, confirm, country, first_name, last_name, type, dob])) {
    if(password === confirm) {
      if(functions.verifyEmail(email)) {
        functions.checkEmail(email, function(err) {
          if(err == true) {
            req.flash("error", "That email address has already been taken!")
            res.redirect("/register")
            return;
          }
          var month = dob.format('M');
          var day = dob.format('D');
          var year = dob.format('YYYY');
          
          var legal_entity = {
            first_name: first_name,
            last_name: last_name,
            dob: {
              day: day,
              month: month,
              year: year
            },
            type: type
          }
          var code = uniqid()
          stripe.accounts.create({
            country: country,
            type: "custom"
          }).then(function(acct) {
            var newUser = new User({
              stripeCustomerToken: acct.id,
              email: email,
              password: password,
              country: country,
              verification: {
                code: code,
                redeemed: false
              }
            })
            newUser.save(function(err) {
              if(err) {
                console.log(err);
                res.redirect("/")
              } else {
                stripe.accounts.update(acct.id,
                  {
                    tos_acceptance: {
                      date: Math.floor(Date.now() / 1000),
                      ip: req.connection.remoteAddress
                    },
                    legal_entity: legal_entity
                  }, function(err, ac) {
                    if(err) {
                      req.flash("error", "Something went wrong while we were creating your account!")
                      res.redirect("/register")
                      console.log(err)
                      return;
                    }
                  });
                
                var hostname = "http://" + req.headers.host;
                
                nodemailer.sendVerificationMessage(email, code, hostname, function(err, doc) {
                  if(!err && doc) {
                    req.flash("success", "Please check your email to verify your account.")
                    res.redirect("/register")
                  } else {
                    console.log(err)
                    req.flash("error", "Something went wrong while sending the verification code to your email address!")
                    res.redirect("/register")
                  }
                });
                
              }
            });
          }).catch(function(err) {
            console.log(err)
            req.flash("error", "Something went wrong")
            res.redirect("/")
          })
        });
      } else {
        req.flash("error", "Incorrect email.")
        res.redirect("/register")
      }
    } else {
      req.flash("error", "Passwords do not match!")
      res.redirect("/register")
    } 
  } else {
    req.flash("error", "Please fill in all fields.")
    res.redirect("/register")
  }
});

module.exports = router;
