var express = require('express');
var router = express.Router();
var functions = require('../../utils/functions')

/* GET users listing. */
router.get('/', function(req, res, next) {
  var stripe = req.app.locals.stripe;
  functions.reqUser(req, function(doc) {
    var advanced = {
      valid:false,
      id:undefined
    };
    var promise = new Promise(function(resolve, reject) {
      var arr = []
      if(doc.customerToken) {
        stripe.subscriptions.list(
          {"customer" : doc.customerToken},
          function(err, subscriptions) {
            if(err) {
              reject("Error has occured")
            } else {
              if(subscriptions.data.length != 0) {
                for(var i = 0; i < subscriptions.data.length; i++) {
                  var sub = subscriptions.data[i]
                  if(sub.plan.id = "advanced") {
                    advanced = {
                      valid: true,
                      id: sub.id
                    }
                  }
                    arr.push(sub)
                  if(i+1 == subscriptions.data.length) {
                    resolve(arr)
                  }
                }
              } else {
                resolve(arr)
              }
            }
        });        
      } else {
        resolve(arr)
      }
    })
    promise.then(function(arr) {
      res.render('user/plans', {
        keyPublishable: req.app.locals.keyPublishable,
        title: 'Plans', 
        description: 'Your current plans with us here at Ticketer.',
        route: req.originalUrl,
        error: req.flash('error'),
        success: req.flash('success'),
        user: doc,
        subscriptions: arr,
        advanced: advanced
      });
    }).catch(function(err){
      req.flash("error", "Something went wrong while retrieving your subscriptions.")
      res.redirect("/panel")
    });
  });
});

module.exports = router;
