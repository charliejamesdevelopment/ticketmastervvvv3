var express = require('express');
var functions = require("../../utils/functions")
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render('user/login', { 
      title: 'Login', 
      route: req.originalUrl,
      error: req.flash('error'),
      success: req.flash('success'),
      user: null
  });
});

router.post('/', function(req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    
    functions.verifyUser(email, password, function(err, match) {
      if(!err && match) {
        req.session.email = email;
        req.flash("success", "You have been logged in!")
        res.redirect("/panel")
      } else {
        req.flash("error", err)
        res.redirect("/login")
      }
    })
});

module.exports = router;
