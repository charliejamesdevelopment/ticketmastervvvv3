var express = require('express');
var router = express.Router();
var functions = require('../../utils/functions')
var validate = require("validate.js")
var Event = require("../models/event")

/* GET users listing. */
router.get('/:id', function(req, res, next) {
  functions.reqUserAll(req, function(doc) {
    functions.getEvent(req.params.id, function(event) {
      if(event) {
        if(doc._id.equals(event.author)) {
          res.render('user/edit_event_wizard', { 
            title: 'Edit Events', 
            description: 'Edit an Event with our wizard.',
            route: req.originalUrl,
            error: req.flash('error'),
            success: req.flash('success'),
            event: event,
            user: doc
          });        
        } else {
          req.flash("error", "You cannot access that event.")
          res.redirect("/panel/my_events")
        }
      } else {
        req.flash("error", "This event was not found!")
        res.redirect("/panel/my_events")
      }
    })
  });
});

router.post('/:id', function(req, res, next) {
  var name = req.body.name
  var description = req.body.description
  var total_tickets = req.body.total_tickets
  var start = req.body.start
  var end = req.body.end
  var location = req.body.location;
  var id = req.params.id;
  var max_tickets = req.body.max_tickets
  var cover_image = req.body.cover_image
  if(name != "" && total_tickets != "" && max_tickets != "" && start != "" && end != "") {
      var constraints = {
        name: {length: {minimum: 5, maximum: 64}},
        location: {length: {minimum: 3, maximum: 64}}
      };
      var loc_val = validate({location: location}, constraints);
      if(loc_val != undefined) {
          req.flash("error", location.location)
          res.redirect("/panel/my_events/create")
          return;
      }
      var tickets = []
      var promise = new Promise(function(resolve, reject) {
        for(var i = 1; i <= total_tickets; i++) {
          if(req.body["ticket_name_" + i] != undefined) {
            var name = req.body["ticket_name_" + i]
            var desc = req.body["ticket_desc_" + i]
            var price = req.body["ticket_price_" + i]
            if(name != "" && desc != "" && price != "") {
              var val_name = validate({name: name}, constraints);
              var val_price = validate({price: price}, {price: {numericality: true}});
              var val_max = validate({max_tickets: max_tickets}, {max_tickets: {numericality: true}});
              if(val_max != undefined) {
                reject("Please make sure max tickets is a integer.")
              }
              if(val_name != undefined && val_price != undefined) {
                reject("Make sure the name of a ticket is more than 5 characters, and less than 64. Also make sure price is an integer.")
              } else {
                tickets.push({
                  name: name,
                  description: desc,
                  price: price
                })
                
                if(i == total_tickets) {
                  resolve()
                }
              }
            } else {
              reject("Please fill in all fields for Step 2.")
            } 
          } else {
            if(i == total_tickets) {
                resolve()
            } else {
              continue;
            }
          }
        }
      });
      
      promise.then(function(resolve) {
        const n_tickets = tickets
        functions.reqUserAll(req, function(doc) {
          Event.findOne({_id : id}, function(err, ev) {
            if(!err && ev) {
              if(doc._id.equals(ev.author)) {
                ev.location = location;
                ev.name = name;
                ev.start = start;
                if(cover_image != "") { ev.cover_image = cover_image; } 
                ev.end = end;
                ev.description = description;
                ev.tickets = n_tickets;
                ev.max_tickets = max_tickets
                
                ev.save(function(err) {
                  if(!err) {
                    req.flash("success", "Your event has been edited.")
                    res.redirect("/panel/my_events/")
                  } else {
                    console.log(err)
                  }
                })
              } else {
                req.flash("error", "You do not own this event.")
                res.redirect("/panel/my_events/")
              }
            }
          })
          
        })
      }).catch(function(reject) {
          if(reject) {
            req.flash("error", reject)
            res.redirect("/panel/my_events/edit/" + id)
          }
        })
  } else {
    req.flash("error", "Please fill in all fields for Step 1.")
    res.redirect("/panel/my_events/edit/" + id)
  }
});

module.exports = router;
