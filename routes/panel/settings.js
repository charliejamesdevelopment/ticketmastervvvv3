var express = require('express');
var router = express.Router();
var functions = require('../../utils/functions')
var https = require("https")

/* GET users listing. */
router.get('/', function(req, res, next) {
  var stripe = req.app.locals.stripe;
  functions.reqUser(req, function(doc) {
    stripe.accounts.retrieve(
      doc.stripeCustomerToken,
      function(err, account) {
        if(!err && account) {
          if(account.legal_entity.verification.status != "verified") {
            res.render('user/account_settings', { 
              title: 'My Settings', 
              description: 'Change your account settings',
              route: req.originalUrl,
              error: req.flash('error'),
              success: req.flash('success'),
              user: doc,
              customer: {
                full: account
              }
            });  
          } else {
            req.flash("error", "Your identity has already been verified")
            req.flash("error", " you must contact a member of staff to manually change your settings.")
            res.redirect("/panel")
          }
        } else {
          res.redirect("/")
        }
      }
    );

  });
});

module.exports = router;
