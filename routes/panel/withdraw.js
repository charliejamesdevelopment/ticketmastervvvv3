var express = require('express');
var router = express.Router();
var functions = require('../../utils/functions')
var https = require("https")

var getCountryCurrency = function(code, cb) {
  var url = "https://restcountries.eu/rest/v2/alpha/" + code;
  https.get(url, res => {
    res.setEncoding("utf8");
    let body = "";
    res.on("data", data => {
      body += data;
    });
    res.on("end", () => {
      body = JSON.parse(body);
      cb(body.currencies)
    });
  });
}

/* GET users listing. */
router.get('/', function(req, res, next) {
  var stripe = req.app.locals.stripe;
  functions.reqUser(req, function(doc) {
    stripe.balance.retrieve({
      stripe_account: doc.stripeCustomerToken
    }, function(err, balance) {
      functions.getBalances(balance, function(document) {
        const bal = document.available
        const bal_pend = document.pending
        const stripeTok = doc.stripeCustomerToken;
        getCountryCurrency(doc.country, function(result) {
          var balance = result[0].symbol + (parseInt(bal) / 100);
          var balance_pend = result[0].symbol + (parseInt(bal_pend) / 100);
          stripe.accounts.retrieve(
            stripeTok,
            function(err, account) {
              if(!err && account) {
                res.render('user/financial_account', { 
                  title: 'My Revenue', 
                  description: 'View your current balance, and withdraw money.',
                  route: req.originalUrl,
                  error: req.flash('error'),
                  success: req.flash('success'),
                  user: doc,
                  customer: {
                    full: account,
                    balance: err ? 0 : balance,
                    bal_pend: err ? 0 : balance_pend
                  },
                  verification_fields : account.verification.fields_needed
                }); 
              } else {
                res.redirect("/")
              }
            }
          );
        })        
      })
    });

  });
});

module.exports = router;
