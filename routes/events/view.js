var express = require('express');
var router = express.Router();
var functions = require('../../utils/functions')

router.get('/:id', function(req, res, next) {
    functions.getEvent(req.params.id, function(doc) {
        if(doc) {
            functions.soldOut(req.params.id, doc.max_tickets, function(soldOut, amount) {
                res.render('events/view', { 
                  title: 'View ' + doc.name, 
                  route: req.originalUrl,
                  error: req.flash('error'),
                  success: req.flash('success'),
                  user: null,
                  event: doc,
                  sold_out: soldOut,
                  tickets_sold: amount
                });                   
            })
        } else {
            res.redirect("/")
        }
    })
});

module.exports = router;
