var express = require('express');
var router = express.Router();
var functions = require('../../utils/functions')
var nodemailer = require('../../utils/nodemailer')
var Ticket = require("../models/ticket")

router.post('/:id/:ticket', function(req, res) {
    var quantity = req.body.quantity;
    var email = req.body.email;
    var source = req.body.stripeToken;
    var stripe = req.app.locals.stripe;
    functions.getEvent(req.params.id, function(doc) {
        functions.soldOut(doc._id, doc.max_tickets, function(soldOut) {
            if(soldOut) {
                req.flash("error", doc.name + " has run out of tickets! Sorry!")
                res.redirect("/event_handler/view_event/" + doc._id)   
            } else {
                Ticket.find({author: doc._id}, function(err, ticks) {
                    if(!err && doc) {
                        if(quantity > doc.max_tickets - ticks.length) {
                            req.flash("error", "There are only " + (doc.max_tickets - ticks.length) + " ticket(s) left, and you tried to purchase " + quantity + " tickets!") 
                            res.redirect("/event_handler/view_event/" + doc._id)      
                        } else {
                            functions.getUserID(doc.author, function(user) {
                                if(user) {
                                    if(!doc || !user) {
                                        req.flash("error", "Cannot be purchased right now!")
                                        res.redirect("/")
                                    } else {
                                        var ticket = {}
                                        var promise = new Promise(function(resolve, reject) {
                                            for(var i = 0; i < doc.tickets.length; i++) {
                                                var tick = doc.tickets[i]
                                                if(tick._id.equals(req.params.ticket)) {
                                                    ticket = tick;
                                                    resolve()
                                                }
                                                
                                                if(i+1 == doc.tickets.length) {
                                                    reject()
                                                }
                                            }
                                        })
                                        promise.then(function() {
                                            var price = ticket.price;
                                            var amount = price * quantity * 100;
                                            stripe.charges.create({
                                              amount: amount,
                                              currency: "usd",
                                              source: source,
                                              description: "Charge for " + email,
                                              application_fee: parseInt((amount / 100) * 1.9 + 30),
                                              destination: {
                                                account: user.stripeCustomerToken,
                                              },
                                            }, function(err, charge) {
                                              if(!err && charge) {
                                                    new Promise(function(resolve) {
                                                        var ids = []
                                                        for(var i = 0; i < quantity; i++) {
                                                            new Ticket({
                                                                buyer: email,
                                                                author: doc._id,
                                                                user_author: doc.author,
                                                                ticket: ticket._id,
                                                                txn_id: charge._id
                                                            }).save(function(err, obj) {
                                                                if(!err && obj) {
                                                                    ids.push(obj._id)
                                                                    if(ids.length == quantity) {
                                                                        resolve(ids)
                                                                    }
                                                                }
                                                            })
                                                        }                                    
                                                    }).then(function(ids) {
                                                        nodemailer.sendTicketPurchaseMessage(email, doc, ticket, quantity, ids, function() {
                                                            req.flash("success", "Successfully made purchase, please visit your email to receive your tickets.")
                                                            res.redirect("/event_handler/view_event/" + req.params.id)    
                                                        })                                    
                                                    })
                                              } else {
                                                  req.flash("error", "Something went wrong, sorry! Please try again.")
                                                  res.redirect("/event_handler/purchase_ticket/" + req.params.id + "/" + req.params.ticket)
                                                  console.log(err)
                                              }
                                            });
                                        })
                                    }                
                                }
                            })                            
                        }
                    } else {
                        console.log(err)
                        req.flash("error", doc.name + " has run out of tickets! Sorry!")
                        res.redirect("/event_handler/view_event/" + doc._id)  
                    }
                })
            }
        })
    })
});

module.exports = router;
