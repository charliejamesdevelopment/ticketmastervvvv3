var express = require('express');
var router = express.Router();
var functions = require('../../utils/functions')

router.get('/:id/:ticket', function(req, res, next) {
    functions.getEvent(req.params.id, function(doc) {
        var ticket = {}
        var promise = new Promise(function(resolve, reject) {
            for(var i = 0; i < doc.tickets.length; i++) {
                var tick = doc.tickets[i]
                if(tick._id.equals(req.params.ticket)) {
                    ticket = tick;
                    resolve()
                }
                
                if(i+1 == doc.tickets.length) {
                    reject()
                }
            }
        })
        promise.then(function() {
            functions.soldOut(req.params.id, doc.max_tickets, function(soldOut, amount) {
                if(doc) {
                    res.render('events/purchase', { 
                      title: 'Purchase Ticket', 
                      route: req.originalUrl,
                      error: req.flash('error'),
                      success: req.flash('success'),
                      user: null,
                      event: doc,
                      ticket: ticket,
                      sold_out: soldOut,
                      tickets_sold: amount
                    });              
                } else {
                    res.redirect("/")
                }                              
            })
        })
    })
});

module.exports = router;
