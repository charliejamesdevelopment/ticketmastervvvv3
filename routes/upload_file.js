var express = require('express');
var functions = require('../utils/functions')
var router = express.Router();
var uniqid = require('uniqid')

router.post('/', function(req, res, next) { 
    if (!req.files || req.files.file == undefined) {
        return res.status(400).send({status: "error", message: "No files were uploaded."});
    }

    let sampleFile = req.files.file;
    if(sampleFile.mimetype == "image/png" || sampleFile.mimetype == "image/jpeg" || sampleFile.mimetype == "image/jpg") {
        var name = uniqid()
     
        sampleFile.mv('public/images/' + name + '.jpg' , function(err) {
            if (err) return res.status(500).send({status: "error", message: "Something went wrong!"});
            
            res.status(200).send({status: "ok", path: 'images/' + name + '.jpg'})
        })
    } else {
        res.status(400).send({status: "error", message: "Please only upload PNG, JPEG or JPG files!"});
    }
});

module.exports = router;
