var express = require('express');
var functions = require('../utils/functions')
var router = express.Router();

router.get('/', function(req, res, next) { 
  functions.reqUser(req, function(doc) {
    functions.getSpecialEvents(function(event) {
      res.render('index', { 
        title: 'Home', 
        route: req.originalUrl,
        error: req.flash('error'),
        success: req.flash('success'),
        user: doc,
        random: event.random,
        recent: event.recent
      });      
    })
  });
});

module.exports = router;
