var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next){
    req.flash("success", "You have been logged out.")
    req.session = null;
    res.redirect("/");
    
});

module.exports = router;
